package com.example.addressbook.storage;

import android.content.Context;

import com.example.addressbook.mapper.parser.ContactXmlParser;
import com.example.addressbook.mapper.serializer.ContactGroupXmlSerializer;
import com.example.addressbook.model.Contact;
import com.example.addressbook.model.Group;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ContactStorageBuffer {

    private static final String FILENAME = "contacts.xml";

    @Getter
    private static List<Contact> contacts;

    public static void readContacts(Context context) {
        try (InputStream inputStream = new FileInputStream(new File(getAbsolutePath(context)))){
            ContactXmlParser parser = new ContactXmlParser();
            contacts = parser.parse(inputStream);

        } catch (FileNotFoundException e) {
            if (contacts == null) {
                contacts = new ArrayList<>();
            }
        } catch (Exception e) {
            throw new IllegalStateException("Failed to read or parse contacts from file");
        }
    }

    public static void writeContactsAndGroups(Context context) {
        File file = new File(getAbsolutePath(context));
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            ContactGroupXmlSerializer xmlSerializer = new ContactGroupXmlSerializer();
            xmlSerializer.serialize(outputStream, contacts, Group.values());
        } catch (IOException e) {
            throw new IllegalStateException("Failed to serialize and save contacts or groups");
        }
    }

    private static String getAbsolutePath(Context context) {
        return context.getFilesDir().getPath() + "/" + FILENAME;
    }
}
