package com.example.addressbook;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.addressbook.adapter.ContactAdapter;
import com.example.addressbook.domain.IntentRepeater;
import com.example.addressbook.domain.swipe.OnSwipeTouchHandler;
import com.example.addressbook.domain.swipe.OnSwipeTouchListener;
import com.example.addressbook.model.Contact;
import com.example.addressbook.storage.ContactStorageBuffer;

import java.util.List;
import java.util.Optional;

import static android.widget.Toast.LENGTH_SHORT;
import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

public class MainActivity extends AppCompatActivity
        implements OnSwipeTouchListener, OnClickListener, OnItemClickListener, IntentRepeater<Contact> {

    private ContactAdapter adapter;

    @Override
    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ContactStorageBuffer.readContacts(this);
        List<Contact> contacts = ContactStorageBuffer.getContacts();

        adapter = new ContactAdapter(this, contacts);

        ListView contactList = findViewById(R.id.contact_list);
        contactList.setOnTouchListener(new OnSwipeTouchHandler(this, contactList, this));
        contactList.setOnItemClickListener(this);
        contactList.setAdapter(adapter);

        Button addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(this);
    }

    @Override
    public void onSwipeLeftOrRight(int position) {
        adapter.remove(position);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_button) {
            Contact contact = Contact.builder().build();
            Intent intent = excerptObject(contact, this, AddEditContactActivity.class);
            startActivityForResult(intent, 201);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Contact contact = (Contact) parent.getItemAtPosition(position);
        if (position != NO_POSITION) {
            Intent intent = excerptObject(contact, this, AddEditContactActivity.class);
            startActivityForResult(intent, 200);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            Contact contactResponse = extractObject(intent);

            Optional<Contact> contact = adapter.findById(contactResponse.getId());
            if (contact.isPresent()) {
                int position = adapter.getPosition(contact.get());
                adapter.update(position, contactResponse);
            } else {
                adapter.insert(contactResponse);
            }
            Toast.makeText(this, "Kontakt został zapisany", LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Kontakt nie został zapisany", LENGTH_SHORT).show();
        }
    }
}
