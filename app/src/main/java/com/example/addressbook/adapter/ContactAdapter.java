package com.example.addressbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.addressbook.R;
import com.example.addressbook.model.Contact;
import com.example.addressbook.storage.ContactStorageBuffer;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class ContactAdapter extends ArrayAdapter<Contact> {

    private final List<Contact> contacts;

    public ContactAdapter(@NonNull Context context, @NonNull List<Contact> contacts) {
        super(context, 0, contacts);
        this.contacts = contacts;
    }

    public Optional<Contact> findById(final UUID id) {
        return contacts.stream()
                .filter(contact -> contact.getId().equals(id))
                .findFirst();
    }

    public void insert(Contact object) {
        super.insert(object, contacts.size());
    }

    public void update(int position, Contact object) {
        if (object.isEqualTo(getItem(position))) {
            remove(position);
            insert(object, position);
        }
    }

    public void remove(int position) {
        super.remove(getItem(position));
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        ContactStorageBuffer.writeContactsAndGroups(getContext());
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Contact contact = Objects.requireNonNull(getItem(position));

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item, parent, false);
        }
        TextView title = convertView.findViewById(R.id.noteTitle);
        TextView content = convertView.findViewById(R.id.noteContent);

        title.setText(contact.toString());
        content.setText(contact.getPhoneNumber());
        return convertView;
    }
}
