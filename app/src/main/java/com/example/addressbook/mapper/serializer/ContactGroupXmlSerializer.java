package com.example.addressbook.mapper.serializer;

import android.util.Xml;

import com.example.addressbook.model.Contact;
import com.example.addressbook.model.Group;

import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import lombok.SneakyThrows;

public class ContactGroupXmlSerializer {

    private static final String XML_FEATURE = "http://xmlpull.org/v1/doc/features.html#indent-output";

    public void serialize(FileOutputStream outputStream, List<Contact> contacts, Group[] groups) {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        try {
            xmlSerializer.setOutput(outputStream, StandardCharsets.UTF_8.name());

            xmlSerializer.startDocument(StandardCharsets.UTF_8.name(), true);
            xmlSerializer.setFeature(XML_FEATURE, true);

            XmlSerializerDecorator serializer = new XmlSerializerDecorator(xmlSerializer);
            processContactSerialization(serializer, contacts);
            processGroupSerialization(serializer, groups);

            xmlSerializer.endDocument();
            xmlSerializer.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows(value = IOException.class)
    private void processContactSerialization(XmlSerializerDecorator serializer, List<Contact> contacts) {
        serializer.startTag("contacts");
        for (Contact contact : contacts) {
            serializer.startTag("contact")
                    .attribute("id", contact.getId())
                    .taggedText("groupId", contact.getGroup().getId())
                    .taggedText("firstName", contact.getFirstName())
                    .taggedText("lastName", contact.getLastName())
                    .taggedText("gender", contact.getGender())
                    .taggedText("address", contact.getAddress())
                    .taggedText("phoneNumber", contact.getPhoneNumber())
                    .taggedText("website", contact.getWebsite())
                    .taggedText("email", contact.getEmail());
            serializer.endTag("contact");
        }
        serializer.endTag("contacts");
    }

    @SneakyThrows(value = IOException.class)
    private void processGroupSerialization(XmlSerializerDecorator serializer, Group[] groups) {
        serializer.startTag("groups");
        for(Group group : groups) {
            serializer.startTag("group")
                    .attribute("id", group.getId())
                    .taggedText("name", group.name());
            serializer.endTag("group");
        }
        serializer.endTag("groups");
    }
}
