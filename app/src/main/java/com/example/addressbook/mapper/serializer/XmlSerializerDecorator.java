package com.example.addressbook.mapper.serializer;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

import lombok.AllArgsConstructor;

@AllArgsConstructor
class XmlSerializerDecorator {

    private XmlSerializer serializer;

    XmlSerializerDecorator taggedText(String tag, Object text) throws IOException {
        return startTag(tag).text(text).endTag(tag);
    }

    XmlSerializerDecorator startTag(String tag) throws IOException {
        serializer.startTag("", tag);
        return this;
    }

    XmlSerializerDecorator attribute(String attribute, Object text) throws IOException {
        serializer.attribute("", attribute, text.toString());
        return  this;
    }

    XmlSerializerDecorator text(Object text) throws IOException {
        serializer.text(text.toString());
        return this;
    }

    XmlSerializerDecorator endTag(String tag) throws IOException {
        serializer.endTag("", tag);
        return this;
    }
}
