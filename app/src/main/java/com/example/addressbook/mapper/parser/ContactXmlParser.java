package com.example.addressbook.mapper.parser;

import com.example.addressbook.model.Contact;
import com.example.addressbook.model.Gender;
import com.example.addressbook.model.Group;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.SneakyThrows;

public class ContactXmlParser {

    public List<Contact> parse(InputStream inputStream) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);

            XmlPullParser parser = factory.newPullParser();
            parser.setInput(inputStream, null);

            return processParsing(parser);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to parse XML");
        }
    }

    @SneakyThrows(value = {XmlPullParserException.class, IOException.class})
    private List<Contact> processParsing(XmlPullParser parser) {
        List<Contact> contacts = new ArrayList<>();
        Contact.ContactBuilder builder = null;

        int eventType;
        while ((eventType = parser.next()) != XmlPullParser.END_DOCUMENT) {
            String name;
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equals("contact")) {
                        builder = Contact.builder();
                        builder.id(UUID.fromString(parser.getAttributeValue(null, "id")));
                    } else if (builder != null) {
                        processMapping(builder, name, parser);
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if ((builder != null) && "contact".equals(parser.getName())) {
                        contacts.add(builder.build());
                        builder = null;
                    }
                    break;
            }
        }
        return contacts;
    }

    @SneakyThrows(value = {XmlPullParserException.class, IOException.class})
    private void processMapping(Contact.ContactBuilder builder, String name, XmlPullParser parser) {
        switch (name) {
            case "groupId":
                builder.group(Group.ofId(UUID.fromString(parser.nextText())));
                break;
            case "firstName":
                builder.firstName(parser.nextText());
                break;
            case "lastName":
                builder.lastName(parser.nextText());
                break;
            case "gender":
                builder.gender(Gender.valueOf(parser.nextText()));
                break;
            case "address":
                builder.address(parser.nextText());
                break;
            case "phoneNumber":
                builder.phoneNumber(parser.nextText());
                break;
            case "website":
                builder.website(parser.nextText());
                break;
            case "email":
                builder.email(parser.nextText());
                break;
        }
    }
}
