package com.example.addressbook.domain.swipe;

public interface OnSwipeTouchListener {

    void onSwipeLeftOrRight(int position);
}
