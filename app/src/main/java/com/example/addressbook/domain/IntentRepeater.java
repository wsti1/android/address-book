package com.example.addressbook.domain;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;
import java.util.Objects;

public interface IntentRepeater<T> {

    default Intent excerptObject(T object) {
        Intent intent = new Intent();
        intent.putExtra(getGenericTypeName(), (Serializable) object);
        return intent;
    }

    default Intent excerptObject(T object, Context context, Class<? extends AppCompatActivity> activity) {
        Intent intent = new Intent(context, activity);
        intent.putExtra(getGenericTypeName(), (Serializable) object);
        return intent;
    }

    @SuppressWarnings("unchecked")
    default T extractObject(Intent intent) {
        return (T) Objects.requireNonNull(intent)
                .getSerializableExtra(getGenericTypeName());
    }

    default String getGenericTypeName() {
        return Objects.requireNonNull(getClass().getGenericSuperclass())
                .getTypeName();
    }
}
