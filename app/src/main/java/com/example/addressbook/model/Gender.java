package com.example.addressbook.model;

import androidx.annotation.NonNull;

import lombok.Getter;

@Getter
public enum Gender {

    MAN,
    WOMAN;

    @NonNull
    @Override
    public String toString() {
        return this.name();
    }
}
