package com.example.addressbook.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.StringJoiner;
import java.util.UUID;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Contact implements Serializable {

    @Builder.Default
    private UUID id = UUID.randomUUID();
    @Builder.Default
    private Group group = Group.NONE;
    private String firstName;
    private String lastName;
    @Builder.Default
    private Gender gender = Gender.MAN;
    private String address;
    private String phoneNumber;
    private String website;
    private String email;

    public boolean isEqualTo(Contact contact) {
        return (contact != null) && id.equals(contact.getId());
    }

    @NonNull
    @Override
    public String toString() {
        return new StringJoiner(" ")
                .add(firstName)
                .add(lastName)
                .add(":")
                .add(group.toString())
                .toString();
    }
}
