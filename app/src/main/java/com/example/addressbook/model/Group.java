package com.example.addressbook.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Group implements Serializable {

    NONE(UUID.fromString("502c6c43-3913-4384-b9ae-fb4a773a27e2")),
    WORK(UUID.fromString("5a2eb9d1-4903-4814-8d0b-52757f28f7e5")),
    HOME(UUID.fromString("ec2a59bd-31a2-475f-93e9-59d745acfe54"));

    private UUID id;

    @NonNull
    @Override
    public String toString() {
        return this.name();
    }

    public static Group ofId(UUID id) {
        for (Group group : values()) {
            if (group.id.equals(id)) {
                return group;
            }
        }
        throw new IllegalArgumentException("Not found group by id: " + id.toString());
    }

    public static List<String> names() {
        return Arrays.stream(values())
                .map(Group::toString)
                .collect(Collectors.toList());
    }
}
