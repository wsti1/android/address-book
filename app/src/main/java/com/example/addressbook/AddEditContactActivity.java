package com.example.addressbook;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.addressbook.domain.IntentRepeater;
import com.example.addressbook.model.Contact;
import com.example.addressbook.model.Gender;
import com.example.addressbook.model.Group;

import java.util.UUID;

public class AddEditContactActivity extends AppCompatActivity
        implements View.OnClickListener, IntentRepeater<Contact> {

    private UUID contactId;

    private EditText firstNameText;
    private EditText lastNameText;
    private EditText phoneNumberText;
    private EditText addressText;
    private EditText websiteText;
    private EditText emailText;

    private RadioGroup genderGroup;
    private AutoCompleteTextView groupText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_contact);

        Button exitButton = findViewById(R.id.exit_button);
        exitButton.setOnClickListener(this);

        Button saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);

        firstNameText = findViewById(R.id.first_name_text);
        lastNameText = findViewById(R.id.last_name_text);
        phoneNumberText = findViewById(R.id.phone_number_text);
        addressText = findViewById(R.id.address_text);
        websiteText = findViewById(R.id.website_text);
        emailText = findViewById(R.id.email_text);
        genderGroup = findViewById(R.id.gender_group);

        groupText = findViewById(R.id.group_text);
        groupText.setOnClickListener(this);
        groupText.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, Group.names()));

        Contact contact = extractObject(getIntent());
        initializeFields(contact);
    }

    private void initializeFields(Contact contact) {
        contactId = contact.getId();
        firstNameText.setText(contact.getFirstName());
        lastNameText.setText(contact.getLastName());
        phoneNumberText.setText(contact.getPhoneNumber());
        addressText.setText(contact.getAddress());
        websiteText.setText(contact.getWebsite());
        emailText.setText(contact.getEmail());
        groupText.setText(contact.getGroup().toString(), false);
        if (Gender.MAN.equals(contact.getGender())) {
            genderGroup.check(R.id.man_radio);
        } else {
            genderGroup.check(R.id.woman_radio);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.group_text:
                groupText.showDropDown();
                break;
            case R.id.save_button:
                new AlertDialog.Builder(this)
                        .setTitle("Zapisz i wyjdź")
                        .setMessage("Czy na pewno chcesz zapisać zmiany i wyjść?")
                        .setNegativeButton("Nie", (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Tak", (dialog, which) -> {
                            Contact contact = createContact();
                            setResult(RESULT_OK, excerptObject(contact));
                            finish();
                        })
                        .show();
                break;
            case R.id.exit_button:
                new AlertDialog.Builder(this)
                        .setTitle("Wyjście")
                        .setMessage("Czy na pewno chcesz wyjść i utracić zmiany?")
                        .setNegativeButton("Nie", (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Tak", (dialog, which) -> finish())
                        .show();
                break;
        }
    }

    private Contact createContact() {
        return Contact.builder()
                .id(contactId)
                .firstName(String.valueOf(firstNameText.getText()))
                .lastName(String.valueOf(lastNameText.getText()))
                .phoneNumber(String.valueOf(phoneNumberText.getText()))
                .address(String.valueOf(addressText.getText()))
                .website(String.valueOf(websiteText.getText()))
                .email(String.valueOf(emailText.getText()))
                .gender(Gender.valueOf((String) getSelectedGender().getTag()))
                .group(Group.valueOf(String.valueOf(groupText.getText())))
                .build();
    }

    private RadioButton getSelectedGender() {
        return findViewById((genderGroup).getCheckedRadioButtonId());
    }
}

